# -*- coding: utf-8 -*-

from django.contrib.auth import login as django_login, authenticate, logout as django_logout
from django.shortcuts import *
from settings import *
from django.core.urlresolvers import reverse
from django.contrib import messages
from tadese.models import Configuracion,Cuotas
LOGIN_REDIRECT_URL='/'
def login(request):
    error = None
    try:
        sitio = Configuracion.objects.get(id=MUNI_ID)
    except Configuracion.DoesNotExist:
        sitio = None

    if sitio <> None:
      unico_padr = (sitio.ver_unico_padron == 'S')
      #unico_padr = False
      if sitio.mantenimiento == 1:
        return render_to_response('mantenimiento.html', {'dirMuni':MUNI_DIR,'sitio':sitio},context_instance=RequestContext(request))
    else:
      unico_padr = False

    if request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
          if user.is_active:
            django_login(request, user)
            if user.get_profile().tipoUsr==0:
                request.session["usuario"] = request.POST['username']
                if unico_padr:
                  try:
                        padr = Cuotas.objects.filter(padron=request.POST['username'],estado=0).order_by('-id_cuota')[0]
                        LOGIN_REDIRECT_URL = reverse('ver_cuotas', kwargs={'idp':padr.id_padron})
                  except IndexError:
                        padr = None   
                else:
                  LOGIN_REDIRECT_URL = reverse('padrones_responsable')
            elif user.get_profile().tipoUsr==1:
                LOGIN_REDIRECT_URL = reverse('padrones_estudio')
            return HttpResponseRedirect(LOGIN_REDIRECT_URL)
          else:
          ## invalid login
           error = "Usuario/Password incorrectos."
        else:
          ## invalid login
           error = "Usuario/Password incorrectos."
          #return direct_to_template(request, 'invalid_login.html')
    if error:
      messages.add_message(request, messages.ERROR,u'%s' % (error))

    return render_to_response('index.html', {'dirMuni':MUNI_DIR,'sitio':sitio},context_instance=RequestContext(request))

def logout(request):
    request.session.clear()
    django_logout(request)
    return HttpResponseRedirect(LOGIN_URL)

def volverHome(request):
    
    if not request.user.is_active:
      return HttpResponseRedirect(LOGIN_URL)
    if request.user.get_profile().tipoUsr==0:
        LOGIN_REDIRECT_URL = reverse('padrones_responsable')
    elif request.user.get_profile().tipoUsr==1:
      LOGIN_REDIRECT_URL = reverse('padrones_estudio')
    return HttpResponseRedirect(LOGIN_REDIRECT_URL)



