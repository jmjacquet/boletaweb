# -*- coding: utf-8 -*-
from django import forms
from tadese.models import Responsables,Cuotas,Configuracion,DriBoleta,DriBoleta_actividades,DriActividades,DriPadronActividades,DriEstudio
from tadese.models import Tributo,DriEstudio,DriDDJJA,DriDDJJA_actividades
from django.forms import ModelForm
from django.forms.models import BaseInlineFormSet
import datetime
from tadese.utilidades import MESES,ANIOS,ADICIONALES
from django.contrib import admin

from django.forms.widgets import TextInput,NumberInput



admin.site.register(Tributo)
admin.site.register(Responsables)
admin.site.register(Cuotas)
admin.site.register(DriEstudio)
admin.site.register(DriActividades)
admin.site.register(DriPadronActividades)
admin.site.register(DriBoleta)
admin.site.register(DriBoleta_actividades)
admin.site.register(DriDDJJA)
admin.site.register(DriDDJJA_actividades)


class LiqDreiBoletaForm(ModelForm):
    id_padron = forms.IntegerField(widget=forms.HiddenInput) 
    anio = forms.IntegerField(widget=forms.HiddenInput) 
    mes = forms.IntegerField(widget=forms.HiddenInput) 
    vencimiento = forms.DateField(widget=forms.HiddenInput) 
    total = forms.DecimalField(widget=forms.HiddenInput,decimal_places=2)
    adic_detalle = forms.CharField(widget=forms.HiddenInput,required=False)
    adic_select = forms.ChoiceField(choices=ADICIONALES, required=False)
    recargo = forms.DecimalField(widget=forms.NumberInput(attrs={'readonly':'readonly'}),required=False,decimal_places=2) 
    adic_monto = forms.DecimalField(widget=forms.NumberInput(attrs={'readonly':'readonly'}),required=False,decimal_places=2) 
    derecho_neto = forms.DecimalField(widget=forms.NumberInput(attrs={'step':0}),decimal_places=2,initial=0.00)
    tasa_salud_publ = forms.DecimalField(widget=forms.NumberInput(attrs={'step':0}),decimal_places=2,initial=0.00)
    retenciones = forms.DecimalField(widget=forms.NumberInput(attrs={'step':0}),decimal_places=2,initial=0.00)
    class Meta:
        model = DriBoleta
        exclude = ['fechapago','id_cuota']

class LiqDreiActivForm(ModelForm):       
    id_actividad = forms.IntegerField(widget=forms.HiddenInput) 
    impuesto = forms.DecimalField(widget=forms.HiddenInput) 
    base = forms.DecimalField(widget=forms.NumberInput(attrs={'step':0}),decimal_places=2,initial=0.00,min_value=0.00) 
    alicuota = forms.DecimalField(widget=forms.NumberInput(attrs={'step':0}),decimal_places=2,initial=0.00,min_value=0.00) 
    activ_descr = forms.CharField(max_length=200, required=False) 
    minimo = forms.DecimalField(widget=forms.NumberInput(attrs={'step':0}),decimal_places=2,initial=0.00,min_value=0.00) 
    class Meta:
        model = DriBoleta_actividades
    
    
    def clean_id_actividad(self):
        value = self.cleaned_data['id_actividad']
        try:
            return DriActividades.objects.get(id_actividad=value)
        except DriActividades.DoesNotExist:
            raise ValidationError('La Actividad %s no existe!!' % value) 

class EstudioForm(ModelForm):    
    numero = forms.CharField(max_length=10,label='Código') 
    denominacion = forms.CharField(max_length=50,label='Denominación')     
    clave = forms.CharField(widget=forms.PasswordInput(render_value = True),max_length=10,label='Contraseña',required=True) 
    email = forms.EmailField(max_length=50,label='E-Mail Contacto') 
    class Meta:
        model = DriEstudio
        exclude = ['id_estudioc','usuario']
        fields = ['numero', 'denominacion', 'email', 'clave']




class ResponsableForm(ModelForm):    
   nombre = forms.CharField(max_length=50,label='Nombre')     


# class DriDclJuradaForm(ModelForm):
#     class Meta:        
#         model = DriDclJurada
#         #exclude = ('id_dcl_jurada','id_padron','padron','tipo','origen','fecha','id_responsable')
#         CHOICES = DriDclJuradaPeriodo.objects.filter(habilitada='S').order_by('anio')
#         widgets = {
#             'anio': forms.Select(choices=( (x.anio, x.anio) for x in CHOICES )),
#             'periodo': forms.Select(choices=MESES),
#             'fecha': forms.DateInput(format='%d/%m/%Y',
#                         attrs={'class': 'datePicker',}),
#             'fecha_confirmado': forms.DateInput(format='%d/%m/%Y',
#                         attrs={'class': 'datePicker',}),
#             }

class AgregarDDJJForm(forms.Form):       
    anio = forms.ChoiceField(choices=ANIOS)

class DriDDJJAForm(ModelForm):
    id_padron = forms.IntegerField(widget=forms.HiddenInput) 
    anio = forms.IntegerField(widget=forms.HiddenInput)  
    total_imponible = forms.DecimalField(widget=forms.NumberInput(attrs={'readonly':'readonly'}),required=False,decimal_places=2) 
    total_impuestos = forms.DecimalField(widget=forms.NumberInput(attrs={'readonly':'readonly'}),required=False,decimal_places=2) 
    total_adicionales = forms.DecimalField(widget=forms.NumberInput(attrs={'readonly':'readonly'}),required=False,decimal_places=2) 
    
    class Meta:
        model = DriDDJJA
        exclude = ['fecha_carga','fecha_confirmado','fecha_impresa']    

class DriDDJJA_actividadesForm(ModelForm):      
    periodo = forms.IntegerField(widget=forms.NumberInput(attrs={'readonly':'readonly'}),required=False) 
    id_boleta = forms.IntegerField(widget=forms.HiddenInput) 
    id_ddjj = forms.IntegerField(widget=forms.HiddenInput) 
    id_actividad = forms.IntegerField(widget=forms.HiddenInput) 
    base = forms.DecimalField(widget=forms.NumberInput(attrs={'step':0}),decimal_places=2,initial=0.00,min_value=0.00) 
    alicuota = forms.DecimalField(widget=forms.NumberInput(attrs={'step':0}),decimal_places=2,initial=0.00,min_value=0.00) 
    minimo = forms.DecimalField(widget=forms.NumberInput(attrs={'step':0}),decimal_places=2,initial=0.00,min_value=0.00) 
    impuesto = forms.DecimalField(widget=forms.HiddenInput) 
    adicionales = forms.DecimalField(widget=forms.NumberInput(attrs={'step':0}),decimal_places=2,initial=0.00,min_value=0.00) 
    activ_descr = forms.CharField(max_length=200, required=False) 
    
    class Meta:
        model = DriDDJJA_actividades

    def clean_id_actividad(self):
        value = self.cleaned_data['id_actividad']
        try:
            return DriActividades.objects.get(id_actividad=value)
        except DriActividades.DoesNotExist:
            raise ValidationError('La Actividad %s no existe!!' % value) 