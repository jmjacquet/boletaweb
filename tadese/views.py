# -*- coding: utf-8 -*-

from django.template import RequestContext,Context
from django.shortcuts import *
from .models import Responsables,Cuotas,Configuracion,DriEstudio,DriPadronActividades,DriBoleta,DriBoleta_actividades,DriActividades,DriEstudioPadron,Sinc,DriDDJJA,DriDDJJA_actividades,WEB_Liquidacion_ctas,WEB_Liquidacion
from django.views.generic import TemplateView,ListView,CreateView,UpdateView
from django.conf import settings
from django.db.models import Count,Sum
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.db import connection
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response,redirect
from django.contrib import messages
from tadese.utilidades import *
from django import http
try:
    import json
except ImportError:
    from django.utils import simplejson as json

from datetime import datetime,date
from dateutil.relativedelta import *
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist

####################################################
# Funciones que utilizan varios procedimientos
####################################################


def listado_responsables(request):
    try:
        resps = Responsables.objects.all()
    except Responsables.DoesNotExist:
        raise Http404
    return render_to_response('padrones_representantes.html',{'resps':resps},context_instance=RequestContext(request))

def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

def padrones_x_estudio(id_estudioc):
    cursor = connection.cursor()
    cursor.execute("SELECT c.*,r.nombre as nombreResp,t.abreviatura as tipoTributo,\
        (select count(cc.id_cuota) from cuotas cc where ((cc.id_padron=c.id_padron)and(cc.fechapago is not null))) as CantPagos,\
        (select count(cc.id_cuota) from cuotas cc where ((cc.id_padron=c.id_padron)and(cc.fechapago is null))) as CantDeuda,\
        (select sum(cc2.saldo) from cuotas cc2 where ((cc2.id_padron=c.id_padron)and(cc2.fechapago is null))) as Deuda,\
        (select count(cc3.id_cuota) from cuotas cc3 where cc3.id_padron=c.id_padron) as TotalCuotas\
        FROM dri_estudio_padron dep \
        JOIN cuotas c on (c.id_padron=dep.id_padron) \
        JOIN tributo t on (c.tributo=t.id_tributo) \
        JOIN responsables r on (r.id_responsable=c.id_responsable) \
        WHERE (dep.id_estudioc =  %s)and(t.id_tributo=6) GROUP BY c.id_padron,c.padron order by r.nombre,c.padron",[id_estudioc])
    padrones = dictfetchall(cursor)
    return padrones

def padrones_x_responsable(idResp,idPadron=None):
    padrones = Cuotas.objects.filter(id_responsable=idResp).order_by('id_responsable__nombre_boleta','padron').values('id_padron','padron','tributo','tributo__descripcion','tributo__abreviatura','id_unidad').annotate(Count('id_padron'))

    if idPadron:
        padrones=padrones.filter(id_padron=idPadron)
    return padrones

def cuotas_x_padron(idResp,idPadron):
    if idResp is None:
        cuotas = Cuotas.objects.filter(id_padron=idPadron)
    else:
        cuotas = Cuotas.objects.filter(id_padron=idPadron,id_responsable=idResp)
    return cuotas


def responsable_del_padron(idPadr):
    try:
        resp = Cuotas.objects.filter(id_padron=idPadr).order_by('-id_cuota')[0].id_responsable
    except Cuotas.DoesNotExist:
        resp = None

# from django.contrib.auth import authenticate
def puedeVerPadron(request,idPadron,idEstudio):
    usr=request.user
    tipoUsr=request.user.get_profile().tipoUsr

    if tipoUsr==0:
        idResp = Cuotas.objects.filter(id_padron=idPadron).select_related('id_responsable').order_by('-id_cuota')[0].id_responsable.id_responsable        
        if not (int(idResp)==int(usr.get_profile().id_responsable)):
            raise http.Http404
    else:
     if tipoUsr==1:
        try:
            estudio = DriEstudioPadron.objects.filter(id_padron=int(idPadron)).select_related('id_responsable').values_list('id_estudioc',flat=True)
            print estudio
        except ObjectDoesNotExist:
            estudio = None   
            raise http.Http404        
        
        if not (int(idEstudio) in estudio):
            raise http.Http404

class TienePermisoMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if (self.get_object().id_estudioc != self.request.user.get_profile().id_estudioc):
            raise http.Http404
        return super(TienePermisoMixin, self).dispatch(request, *args, **kwargs)

##############################################
#      Mixin para cargar las Vars de sistema #
##############################################

class VariablesMixin(object):
    def get_context_data(self, **kwargs):
        context = super(VariablesMixin, self).get_context_data(**kwargs)
        context['idMuni'] = settings.MUNI_ID
        context['dirMuni'] = settings.MUNI_DIR

        try:
            sinc = Sinc.objects.all().order_by('-fecha','-hora')[0]
        except Sinc.DoesNotExist:
            sinc = None
        try:
            sitio = Configuracion.objects.get(id=settings.MUNI_ID)
        except Configuracion.DoesNotExist:
            sitio = None
        context['sitio'] = sitio
        context['sinc'] = sinc
        return context

##############################################
#      Padrones de Estudios Contables        #
##############################################


class EstudiosView(VariablesMixin,TemplateView):
    template_name = 'padrones_estudio.html'
    context_object_name = 'estudios'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):        
        return super(EstudiosView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EstudiosView, self).get_context_data(**kwargs)
        idEst= int(self.request.user.get_profile().id_estudioc)
        try:
            estudio=DriEstudio.objects.get(id_estudioc=idEst)
        except DriEstudio.DoesNotExist:
            estudio = None

        try:
            context['estudio'] = estudio
            p = padrones_x_estudio(idEst)
            context['padr'] = p
            
            context['padron'] = p[0]
            
        except IndexError:
            context['padr'] = None
            context['padron'] = None
        return context

##########################################
#        Padrones de Responsables        #
##########################################

class ResponsablesView(VariablesMixin,TemplateView):
    template_name = 'padrones_representante.html'
    context_object_name = 'padrones'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ResponsablesView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ResponsablesView, self).get_context_data(**kwargs)
        idResp= int(self.request.user.get_profile().id_responsable)
        
        try:
            context['responsable'] = Responsables.objects.get(id_responsable=idResp)
            p = padrones_x_responsable(idResp,None)
            try:
                  sitio = Configuracion.objects.get(id=settings.MUNI_ID)
            except Configuracion.DoesNotExist:
                  sitio = None
            if sitio <> None:
                if (sitio.ver_unico_padron == 'S'):
                 if "usuario" in self.request.session:
                    p = p.filter(padron=self.request.session["usuario"])
                    
            context['padr'] = p 
            context['padron'] = p[0]
        except Responsables.DoesNotExist:
            context['responsable'] = None
        except IndexError:
            context['padr'] = None
            context['padron'] = None
        return context



##################################################
#      Ver cuotas del Padrón seleccionado        #
##################################################

class BusquedaCuotasView(VariablesMixin,ListView):
    template_name = 'cuotas.html'
    context_object_name = 'cuotas'
 
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        idPadron = self.kwargs.get("idp",'0')
        if self.request.user.get_profile().id_estudioc==None:
            idEst=0
        else:
            idEst= int(self.request.user.get_profile().id_estudioc)
        puedeVerPadron(self.request,idPadron,idEst)
        return super(BusquedaCuotasView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        # Tomo el padrón y anio seleccionado y lo filtro para que me muestre las cuotas
        idPadron = self.kwargs.get("idp",'0')
        idResp = cuotas_x_padron(None,idPadron).select_related('id_responsable').order_by('-id_cuota')[0].id_responsable.id_responsable
        
        anio = int(self.kwargs.get("anio",'0'))

        if (anio==0):
            c = cuotas_x_padron(idResp,idPadron).order_by('-vencimiento').select_related('tributo').extra(
                    select={                        
                        'boleta': 'SELECT dri_boleta.id_boleta FROM dri_boleta WHERE dri_boleta.id_cuota=cuotas.id_cuota LIMIT 1',
                        'total': 'SELECT dri_boleta.total FROM dri_boleta WHERE dri_boleta.id_cuota=cuotas.id_cuota LIMIT 1'
                    },
                )

        else:
            c = cuotas_x_padron(idResp,idPadron).filter(anio=anio).select_related('tributo').order_by('-vencimiento').extra(
                    select={                        
                        'boleta': 'SELECT dri_boleta.id_boleta FROM dri_boleta WHERE dri_boleta.id_cuota=cuotas.id_cuota LIMIT 1',
                        'total': 'SELECT dri_boleta.total FROM dri_boleta WHERE dri_boleta.id_cuota=cuotas.id_cuota LIMIT 1'
                    },
                )
        return c

    def get_context_data(self, **kwargs):
        context = super(BusquedaCuotasView, self).get_context_data(**kwargs)
        # En el contrxto pongo el padrón seleccionado asi saco sus características
        idPadron = self.kwargs.get("idp",'0')
        resp = cuotas_x_padron(None,idPadron).select_related('id_responsable').order_by('-id_cuota')[0].id_responsable
        idResp = resp.id_responsable
        anio = int(self.kwargs.get("anio",'0'))
        context['anio']=anio
        context['responsable'] = resp
        context['padr'] = padrones_x_responsable(idResp,None)        
        if idResp:
            if idPadron:
                try:
                    p = padrones_x_responsable(idResp,idPadron)[0]
                except IndexError:
                    p = None
                context['padron']=p
        return context

##########################################################################3



def armarCodBar(cod):
    import imprimirPDF
    b  = imprimirPDF.get_image2(cod)
    return b

def imprimirPDF(request,idc):   
    
    from Code128 import Code128
    from base64 import b64encode
        
    c = Cuotas.objects.get(id_cuota=idc)    
           
    diasExtra = None
    try:
        sitio = Configuracion.objects.get(id=settings.MUNI_ID)
        diasExtra = sitio.diasextravencim

    except Configuracion.DoesNotExist:
        sitio = None
        
    if diasExtra == None:
        diasExtra=0

    hoy = date.today()

    if (hoy >= c.vencimiento):
        vencimiento = hoy + relativedelta(days=diasExtra)   
        vencimiento2 = vencimiento
    else:
        vencimiento = c.vencimiento
        if c.segundo_vencimiento==None:
           vencimiento2 = vencimiento + relativedelta(months=1)
        else:
            vencimiento2 = c.segundo_vencimiento   

    context = Context()    
    context['cuota'] = c
    context['idMuni'] = settings.MUNI_ID
    context['dirMuni'] = settings.MUNI_DIR
    context['fecha'] = datetime.now()
    context['vencimiento'] = vencimiento
    context['codseg'] = c.id_responsable.codseg
    context['sitio'] = sitio
    
    from easy_pdf.rendering import render_to_pdf_response   
 
    if c.tributo.id_tributo == 6 :
       template ='boletas/boleta_drei.html'
       boleta = DriBoleta.objects.filter(id_padron=c.id_padron,anio=c.anio,mes=c.cuota)[0]
       a = DriBoleta_actividades.objects.filter(id_boleta=boleta).select_related('id_actividad')
       totOrig = a.aggregate(Sum('impuesto'))
       context['actividades'] = a
       context['totOriginal'] = totOrig 
       context['boleta'] = boleta
       context['subtotal'] = boleta.total       
       context['vencimiento2'] = vencimiento
       vencimiento2 = vencimiento
       totales = calcularPunitorios(request,c,boleta,boleta.total)
       #totales = calcularPunitorios(request,c,boleta,totOrig['impuesto__sum'])
    else:
       template = 'boletas/boleta_tasas.html'
       totales  = calcularPunitorios(request,c,None,0)
       context['vencimiento2'] = vencimiento2
    
    
    context['punit1'] = totales['punit1']
    context['porc1'] = totales['porc1']
    context['punit2'] = totales['punit2']
    context['porc2'] = totales['porc2']

    cod = ""
    cod += str(sitio.id).rjust(3, "0")          #CODIGO DEL MUNICIPIO
    cod += str(c.tributo.id_tributo).rjust(3, "0") #TRIBUTO
    #Pongo el 2do vencimiento asi quedan los dos iguales con el importe actualizado
    cod += str(vencimiento.strftime("%d%m%y")).rjust(6, "0") #Vencimiento
    cod += str(totales['punit1']).replace(".","").rjust(7, "0") #Importe Actualizado
    cod += str(vencimiento2.strftime("%d%m%y")).rjust(6, "0") #Vencimiento2
    cod += str(totales['punit2']).replace(".","").rjust(7, "0") #Importe2 Actualizado
    cod += str(c.id_cuota).rjust(9, "0") #Id Cuota
    cod += str(c.anio).rjust(4, "0") #Anio
    cod += str(c.cuota).rjust(2, "0") #Cuota    
    cod += str(digVerificador(cod))
    
    path = 'staticfiles/munis/'+settings.MUNI_DIR+'/'
    
    context['codbar'] = armarCodBar(cod)
    context['codigo'] = cod
    
    return render_to_pdf_response(request, template, context)

##################################################
#     Liquidacion DReI                         #
##################################################
from django.forms.models import inlineformset_factory
from .forms import LiqDreiBoletaForm,LiqDreiActivForm,EstudioForm,ResponsableForm,AgregarDDJJForm
from django.db import transaction
from django.contrib.messages import constants as message_constants


class DreiLiquidarCreateView(VariablesMixin, CreateView):
    form_class = LiqDreiBoletaForm
    template_name = 'drei/drei_liquidacion.html'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):        
        return super(DreiLiquidarCreateView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DreiLiquidarCreateView, self).get_context_data(**kwargs)
        idc = int(self.kwargs.get("idc",'0'))
        cuota = Cuotas.objects.get(id_cuota=idc)  
        context['titulo'] = 'Autoliquidación de Boletas de DReI'
        hoy = date.today()

        actividades = DriPadronActividades.objects.filter(id_padron=cuota.id_padron).select_related('id_actividad').filter(Q(fecha_fin__gte=hoy)|Q(fecha_fin=None))
        
        ActividadesFormSet = inlineformset_factory(DriBoleta,DriBoleta_actividades,extra=actividades.count(),can_delete=False,form=LiqDreiActivForm)
        context['cuota'] = cuota
        context['responsable'] = Responsables.objects.get(id_responsable=cuota.id_responsable.id_responsable)
        if self.request.POST:            
            form = LiqDreiBoletaForm(self.request.POST)
            data_activ = []
            for activ in actividades:                
                data_activ.append({'id_actividad':activ.id_actividad.id_actividad,'activ_descr':activ.verDetalleActiv})
                
            context['actividades_formset'] = ActividadesFormSet(self.request.POST,prefix='actividades',initial=data_activ)
        else:
            data_activ = []
            for activ in actividades:
                if activ.monto_minimo is None:
                    minimo = 0
                elif activ.principal != 'S':
                    minimo = 0
                else:
                    minimo =activ.monto_minimo
                    #Le pongo el minimo de la principal a la boleta
                    context['minimo_boleta'] =minimo

                if activ.id_actividad.alicuota is None:
                    alicuota = 0
                else:
                    alicuota =activ.id_actividad.alicuota;
                
                data_activ.append({'id_actividad': activ.id_actividad.id_actividad,'minimo':minimo,'alicuota':alicuota,
                    'activ_descr':activ.verDetalleActiv,'base':'0','impuesto':'0'})
                
            
            data = {'id_padron': cuota.id_padron,'anio': cuota.anio,'mes': cuota.cuota,'vencimiento': cuota.vencimiento,'total':0,
                        'recargo':0,'derecho_neto':0,'tasa_salud_publ':0,'retenciones':0,'adic_monto':0,'id_cuota':cuota.id_cuota}

            form= LiqDreiBoletaForm(data)

            context['form'] = form
            context['actividades_formset'] = ActividadesFormSet(instance=DriBoleta(),prefix='actividades',initial=data_activ)
        return context
 
    def form_valid(self, form):
        context = self.get_context_data()
        actividades_formset = context['actividades_formset']
        cuota = context['cuota']  
        with transaction.commit_on_success():            
            
            if form.is_valid():
                self.object = form.save(commit=False)                
                if actividades_formset.is_valid():
                    activ = actividades_formset.save(commit=False)
                    self.object.id_cuota=cuota
                    form.save()
                    for forma in activ:
                        forma.id_boleta = self.object
                        dria=DriPadronActividades.objects.get(id_padron=self.object.id_padron,id_actividad=forma.id_actividad)                        
                        forma.activ_descr = dria
                        forma.save(self.object)                                       
                else:
                    
                    return self.form_invalid(form)
            else:
                return self.form_invalid(form)
        return super(DreiLiquidarCreateView, self).form_valid(form)
 
    def get_success_url(self):
        idc = int(self.kwargs.get("idc",'0'))
        cuota = Cuotas.objects.get(id_cuota=idc)  
        messages.add_message(self.request, messages.SUCCESS,u'El Período %s/%s fué liquidado exitosamente!. Recuerde Imprimir la Boleta.' % (cuota.cuota,cuota.anio))
        return reverse('ver_cuotas', kwargs={'idp':cuota.id_padron})

    def form_invalid(self, form,):
        context = self.get_context_data()
        actividades_formset = context['actividades_formset']
        return self.render_to_response(
            self.get_context_data(form=form,
                                  actividades_formset=actividades_formset))


class DreiLiquidarUpdateView(VariablesMixin, CreateView):
    form_class = LiqDreiBoletaForm
    template_name = 'drei/drei_liquidacion.html'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):        
        return super(DreiLiquidarUpdateView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DreiLiquidarUpdateView, self).get_context_data(**kwargs)
        idc = int(self.kwargs.get("idc",'0'))
        cuota = Cuotas.objects.get(id_cuota=idc)  
        boleta = DriBoleta.objects.filter(id_padron=cuota.id_padron,anio=cuota.anio,mes=cuota.cuota)[0]
        boleta_activ = DriBoleta_actividades.objects.filter(id_boleta=boleta)
        
        for activ in boleta_activ:
            dria=DriPadronActividades.objects.get(id_padron=boleta.id_padron,id_actividad=activ.id_actividad)
            if dria.principal == 'S':
                #Le pongo el minimo de la principal a la boleta
                context['minimo_boleta'] =dria.monto_minimo

        context['cuota'] = cuota
        context['titulo'] = 'Rectificativa Boletas de DReI'
        
        ActividadesFormSet = inlineformset_factory(DriBoleta,DriBoleta_actividades,can_delete=False,max_num=boleta_activ.count(),form=LiqDreiActivForm)
        context['responsable'] = Responsables.objects.get(id_responsable=cuota.id_responsable.id_responsable)
        
        if self.request.POST:            
            form = LiqDreiBoletaForm(self.request.POST,instance=boleta,)            
            context['form'] = form
            context['actividades_formset'] = ActividadesFormSet(self.request.POST,prefix='actividades',instance=boleta)
        else:
            context['form'] = LiqDreiBoletaForm(instance=boleta)            
            actividades_formset = ActividadesFormSet(prefix='actividades',instance=boleta)            
            context['actividades_formset'] = actividades_formset
        return context
 
    def form_valid(self, form):
        context = self.get_context_data()
        actividades_formset = context['actividades_formset']
        cuota = context['cuota']
        form = context['form']
        with transaction.commit_on_success():            
            if form.is_valid():
                self.object = form.save(commit=False)                
                if actividades_formset.is_valid():
                    activ = actividades_formset.save(commit=False)                    
                    form.save()
                    for forma in activ:                    
                        dria=DriPadronActividades.objects.get(id_padron=self.object.id_padron,id_actividad=forma.id_actividad)                        
                        forma.activ_descr = dria.verDetalleActiv()
                        
                        forma.save()                                      
                else:                    
                    return self.form_invalid(form)
            else:
                return self.form_invalid(form)
        return super(DreiLiquidarUpdateView, self).form_valid(form)
 
    def get_success_url(self):
        idc = int(self.kwargs.get("idc",'0'))
        cuota = Cuotas.objects.get(id_cuota=idc)  
        messages.add_message(self.request, messages.SUCCESS,u'El Período %s/%s fué Rectificado exitosamente!. Recuerde Imprimir la Boleta.' % (cuota.cuota,cuota.anio))
        return reverse('ver_cuotas', kwargs={'idp':cuota.id_padron})

    def form_invalid(self, form,):
        context = self.get_context_data()
        actividades_formset = context['actividades_formset']
        return self.render_to_response(
            self.get_context_data(form=form,
                                  actividades_formset=actividades_formset))

#################################################################
  
def verificarCuota(request,idc):
    if request.is_ajax():
        cuota = Cuotas.objects.get(id_cuota=idc)  
        boleta = DriBoleta.objects.filter(id_padron=cuota.id_padron,anio=cuota.anio,mes=cuota.cuota)
        if boleta:
            data = {'msj': "La boleta %s/%s ya se encuentra liquidada!!" % (cuota.cuota,cuota.anio),'url':""}
        else:
            data = {'msj': "",'url': reverse('drei_liquidarBoleta', kwargs={'idc':idc})}
        return HttpResponse(json.dumps(data), content_type='application/json')


##################################################
#      Ver DDJJ Presentadas                      #
##################################################

class DreiDDJJAList(VariablesMixin,ListView):
    template_name = 'drei/drei_ddjj_listado.html'
    context_object_name = 'ddjj'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        idPadron = self.kwargs.get("idp",'0')
        if self.request.user.get_profile().id_estudioc==None:
            idEst=0
        else:
            idEst= int(self.request.user.get_profile().id_estudioc)
        puedeVerPadron(self.request,idPadron,idEst)
        return super(DreiDDJJAList, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        idPadron = self.kwargs.get("idp",'0')
        ddjj = DriDDJJA.objects.filter(id_padron=idPadron)
        return ddjj

    def get_context_data(self, **kwargs):
        context = super(DreiDDJJAList, self).get_context_data(**kwargs)
        # En el contrxto pongo el padrón seleccionado asi saco sus características
        idPadron = self.kwargs.get("idp",'0')
        context['idp'] = idPadron 
        resp = cuotas_x_padron(None,idPadron).select_related('id_responsable')[0].id_responsable
        idResp = resp.id_responsable
        context['responsable'] = resp 
        if idResp:
            if idPadron:
                try:
                    p = padrones_x_responsable(idResp,idPadron)[0]
                except IndexError:
                    p = None
                context['padron']=p
        
        formBusqueda=AgregarDDJJForm()
        formBusqueda.fields['anio'].initial = date.today().year
        context['form'] = formBusqueda
        context['anios'] = ANIOS
        return context


##################################################
#      DDJJ Anuales                              #
##################################################
from .forms import DriDDJJAForm,DriDDJJA_actividadesForm

class DreiDDJJACreateView(VariablesMixin, CreateView):
    form_class = DriDDJJAForm
    template_name = 'drei/drei_ddjj.html'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):        
        return super(DreiDDJJACreateView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DreiDDJJACreateView, self).get_context_data(**kwargs)
        anio = int(self.kwargs.get("anio",'0'))
        idPadron = self.kwargs.get("idp",'0')
        resp = cuotas_x_padron(None,idPadron).select_related('id_responsable')[0].id_responsable
        idResp = resp.id_responsable
        context['responsable'] = resp 
        if idResp:
            if idPadron:
                try:
                    p = padrones_x_responsable(idResp,idPadron)[0]
                except IndexError:
                    p = None
        
        liquidaciones_drei = DriBoleta.objects.filter(id_padron=idPadron,anio=anio)

        actividades = DriPadronActividades.objects.filter(id_padron=idPadron).select_related('id_actividad').filter(Q(fecha_fin__gte=hoy)|Q(fecha_fin=None))
        cant_actividades = actividades.count()
       
        periodosFormSet = inlineformset_factory(DriDDJJA,DriDDJJA_actividades,can_delete=False,max_num=12*cant_actividades,extra=12*cant_actividades,form=DriDDJJA_actividadesForm)
                
        if self.request.POST:            
            form = DriDDJJAForm(self.request.POST)
            data_activ = []
            for activ in actividades:                
                data_activ.append({'id_actividad':activ.id_actividad.id_actividad,'activ_descr':activ})
            context['periodos_formset'] = periodosFormSet(self.request.POST,prefix='periodos',initial=data_activ)
        else:
            data_periodos = []
            for i in range(13):
                i=i+1;
                #liq=liquidaciones_drei.get(mes=i,anio=anio)
                boleta_activ = DriBoleta_actividades.objects.filter(id_boleta__mes=i,id_boleta__anio=anio,id_boleta__id_padron=idPadron)
                if boleta_activ:
                    for b in boleta_activ:
                        data_periodos.append({'periodo': i,'id_boleta':b.id_boleta.id_boleta,'id_actividad':b.id_actividad.id_actividad,'base':b.base,'minimo':b.minimo,
                                'alicuota':b.alicuota,'impuesto':b.impuesto,'activ_descr':b.activ_descr})
                else:
                    for b in actividades:
                        data_periodos.append({'periodo': i,'id_actividad':b.id_actividad.id_actividad,'base':0,'minimo':b.id_actividad.minimo,
                                'alicuota':b.id_actividad.alicuota,'impuesto':0,'activ_descr':b})
            
            data = {'anio': anio,'id_padron': idPadron,'total_imponible':0,'total_impuestos':0,'total_adicionales':0,}

            form= DriDDJJAForm(data)

            context['form'] = form
            context['periodos_formset'] = periodosFormSet(instance=DriDDJJA(),prefix='periodos',initial=data_periodos)
        return context

 
    def form_valid(self, form):
        context = self.get_context_data()
        periodos_formset = context['periodos_formset']
        cuota = context['cuota']  
        with transaction.commit_on_success():            
            
            if form.is_valid():
                self.object = form.save(commit=False)                
                if actividades_formset.is_valid():
                    activ = periodos_formset.save(commit=False)
                    boleta = DriBoleta.objects.filter(id_padron=cuota.id_padron,anio=cuota.anio,mes=cuota.cuota)[0]
                    self.object.id_boleta=boleta
                    form.save()
                    for forma in activ:
                        forma.id_boleta = self.object.id_boleta
                        dria=DriPadronActividades.objects.get(id_padron=self.object.id_padron,id_actividad=forma.id_actividad)                        
                        forma.activ_descr = dria
                        forma.save(self.object)                                       
                else:
                    
                    return self.form_invalid(form)
            else:
                return self.form_invalid(form)
        return super(DreiDDJJACreateView, self).form_valid(form)
 
    def get_success_url(self):
        idc = int(self.kwargs.get("idc",'0'))
        cuota = Cuotas.objects.get(id_cuota=idc)  
        messages.add_message(self.request, messages.SUCCESS,u'El Período %s/%s fué liquidado exitosamente!. Recuerde Imprimir la Boleta.' % (cuota.cuota,cuota.anio))
        return reverse('ver_cuotas', kwargs={'idp':cuota.id_padron})

    def form_invalid(self, form,):
        context = self.get_context_data()
        periodos_formset = context['periodos_formset']
        return self.render_to_response(
            self.get_context_data(form=form,
                                  periodos_formset=periodos_formset))

##################################################
#      Update de Estudios                        #
##################################################
class EstudiosUpdateView(VariablesMixin,TienePermisoMixin,UpdateView):
    template_name = 'estudio_update.html'
    model = DriEstudio
    success_url = '/'
    form_class = EstudioForm

####################################################
 

def mandarEmailEstudio(request,usrEstudio):
    from smtplib import SMTP
    from email.mime.text import MIMEText as text
    if request.is_ajax:
        message=''                
        try:
            estudio = DriEstudio.objects.filter(usuario=usrEstudio)[0]
        except IndexError:
            estudio = None            
            message = "El usuario es incorrecto."
            return HttpResponse(message)
        
        login_valid = (estudio <> None)
        
        if login_valid:
            to_addr=estudio.email
            clave = estudio.clave
            from_addr = 'contacto@grupoguadalupe.com.ar'
            msg = u'Ud. ha solicitado el envio de su password por email.\nSu password es: %s' % clave

            m = text(msg)

            m['Subject'] = 'Password Requerido (Sistema Liquidacion OnLine)'
            m['From'] = from_addr
            m['To'] = to_addr    
            s = SMTP()
            s.connect('smtp.webfaction.com')
            s.login('grupogua_juanmanuel','qwerty')
            s.sendmail(from_addr, to_addr, m.as_string())
            message="Se envio correctamente el email."            
        return HttpResponse(message)

##################################################################

def calcularPunitorios(request,c,boleta=None,importe=0):
    try:
        cuota = c       
        hoy = date.today()  
        vencimiento = cuota.vencimiento

        if cuota.segundo_vencimiento==None:
           vencimiento2 = vencimiento + relativedelta(months=1)
        else:
            vencimiento2 = cuota.segundo_vencimiento           

        fecha = hoy  

        if cuota.tributo.id_tributo==6 :
            hoy = hoy 
            if cuota.segundo_vencimiento==None:
               vencimiento2 = vencimiento
            else:
               vencimiento2 = cuota.segundo_vencimiento               
               
            if boleta:
                if vencimiento < boleta.fechapago:
                    vencimiento = boleta.fechapago
                if vencimiento2 < boleta.fechapago:
                    vencimiento = boleta.fechapago

            if (hoy<=vencimiento):
                fecha=vencimiento                          
            elif ((hoy>vencimiento)and(hoy<=vencimiento2)):
                fecha=vencimiento2
            else:
                fecha=hoy
                vencimiento2=fecha
            
            por1 = punitorios(cuota,vencimiento,fecha)
            por2 = punitorios(cuota,vencimiento,vencimiento2)
            punit1 = importe * (1+por1) 
            porc1 = punit1 - importe
            punit2 = punit1
            porc2 =  porc1
        
        else:
            if (hoy<=vencimiento):
                fecha=vencimiento                          
            elif ((hoy>vencimiento)and(hoy<=vencimiento2)):
                fecha=vencimiento2
            else:
                fecha=hoy
                vencimiento2=fecha
                
            por1 = punitorios(cuota,vencimiento,fecha)
            por2 = punitorios(cuota,vencimiento,vencimiento2)
            punit1 = cuota.saldo * (1+por1) 
            porc1 = punit1 - cuota.saldo
            punit2 = cuota.saldo * (1+por2)
            porc2 =  punit2 - cuota.saldo       
    except KeyError:
        return HttpResponse('Error') # Error Manejado
    print 'por1:'+str(por1)+' porc1:'+str(porc1)+' punit1:'+str(punit1)+' por2:'+str(por2)+' porc2:'+str(porc2)+' punit2:'+str(punit2)
    return {'punit1': format(punit1, '.2f'), 'porc1': format(porc1, '.2f'),'punit2': format(punit2, '.2f'), 'porc2': format(porc2, '.2f')}

##########################################################################
# Funciones ajax para la liquidacion y cálculo de Punitorios ONLINE

def calcularPunitoriosForm(request,idc):
    try:
        c = Cuotas.objects.get(id_cuota=idc)
        hoy = date.today() 
        por1 = punitorios(c,c.vencimiento,hoy)                
    except KeyError:
        return HttpResponse('Error') # Error Manejado
    if request.is_ajax():          
        return HttpResponse(str(por1))
    else:
        return HttpResponse('Error') # Error Manejado

def generarPunitoriosLiq(request,idp):   
    if request.is_ajax():         
            datos = request.GET.getlist('cuotas[]')            
            cuotasAct = {}            
            if datos:
                for i in datos:
                    c = Cuotas.objects.get(id_cuota=i) 
                    if c.tributo.id_tributo==6:
                        boleta = DriBoleta.objects.filter(id_padron=c.id_padron,anio=c.anio,mes=c.cuota)[0]
                        total  = calcularPunitorios(request,c,boleta,boleta.total)
                    else:
                        total  = calcularPunitorios(request,c,None,0)
                    cuotasAct[i]=total['punit1']
                    print cuotasAct
                    # subtotal = subtotal + float(totales['punit1'])
            return HttpResponse(json.dumps(cuotasAct), content_type = "application/json")

def generarLiquidacion(request,idp):   
    if request.is_ajax():         
            padr = cuotas_x_padron(None,idp)[0]
            id_unidad = padr.id_unidad
            datos = request.GET.getlist('cuotas[]')            
            subtotal = 0
            totNom = 0
            totInt = 0
            totales = {}
            if datos:
                for i in datos:
                    c = Cuotas.objects.get(id_cuota=i)                    
                    if c.tributo.id_tributo==6:
                        boleta = DriBoleta.objects.filter(id_padron=c.id_padron,anio=c.anio,mes=c.cuota)[0]
                        
                        total  = calcularPunitorios(request,c,boleta,boleta.total)
                    else:
                        total  = calcularPunitorios(request,c,None,0)
                    totales[i]=total                    
                    totInt += float(total['porc1'])
                    totNom += float(total['punit1']) - float(total['porc1'])
                    subtotal += float(total['punit1'])
                # Genero la liquidacion    
                diasExtra = Configuracion.objects.get(id=settings.MUNI_ID).diasextravencim
                if diasExtra == None:
                    diasExtra=0
                hoy = date.today() 
                
                venc = hoy + relativedelta(days=diasExtra)   
                liq =WEB_Liquidacion(id_unidad = id_unidad,tipo=1,vencimiento=venc,nominal=totNom,interes = totInt,total = subtotal,\
                    pasado_a_cnv=0,usuario=request.user.username,fecha_punitorios = hoy,punitorios = totInt)
                liq.save()
                for i in datos:
                    c = Cuotas.objects.get(id_cuota=i)  
                    totNom = float(totales[i]['punit1']) - float(totales[i]['porc1'])
                    totInt = float(totales[i]['porc1'])
                    liq_cta = WEB_Liquidacion_ctas(id_liquidacion = liq,id_cuota=c,tributo=c.tributo.id_tributo,nominal=totNom ,interes=totInt)
                    liq_cta.save()
                if liq:                   
                    messages.add_message(request, messages.SUCCESS,u'Se generó la Liquidación Nº %s exitosamente!.'  % (liq.pk))
                
            return HttpResponse(json.dumps(liq.id_liquidacion), content_type = "application/json")

def imprimirPDFLiqWeb(request,id_liquidacion):   
    
    from Code128 import Code128
    from base64 import b64encode
        
    liq = WEB_Liquidacion.objects.get(id_liquidacion=id_liquidacion)    
    liq_ctas = WEB_Liquidacion_ctas.objects.filter(id_liquidacion=id_liquidacion).extra(
                        select = {'total': '(nominal + interes)'},).order_by('-id_cuota')
    
    descrCtas =''                        
    for i in liq_ctas:
        if descrCtas=='':
            descrCtas=(i.id_cuota.cuota+'/'+str(i.id_cuota.anio))
        else:
            descrCtas= descrCtas+' - ' +(i.id_cuota.cuota+'/'+str(i.id_cuota.anio))

    if liq_ctas:
        c = Cuotas.objects.get(id_cuota=liq_ctas[0].id_cuota.id_cuota)


    diasExtra = None
    try:
        sitio = Configuracion.objects.get(id=settings.MUNI_ID)
        diasExtra = sitio.diasextravencim

    except Configuracion.DoesNotExist:
        sitio = None
        
    if diasExtra == None:
        diasExtra=0

    hoy = date.today()

    vencimiento = liq.vencimiento
    vencimiento2 = vencimiento
 
    context = Context()    
    context['liq'] = liq
    context['idMuni'] = settings.MUNI_ID
    context['dirMuni'] = settings.MUNI_DIR
    context['vencimiento'] = vencimiento   
    context['sitio'] = sitio
    context['cuota'] = c
    context['descrCtas'] = descrCtas
    
    from easy_pdf.rendering import render_to_pdf_response   
 
    template ='boletas/boleta_liq.html'
    context['liq_ctas'] = liq_ctas
    context['vencimiento2'] = vencimiento2

    cod = ""
    cod += str(sitio.id).rjust(3, "0")#CODIGO DEL MUNICIPIO
    cod += str('998').rjust(3, "0") #TRIBUTO LIQUIDACION WEB
    #Pongo el 2do vencimiento asi quedan los dos iguales con el importe actualizado
    cod += str(vencimiento.strftime("%d%m%y")).rjust(6, "0") #Vencimiento
    cod += str(liq.total).replace(".","").rjust(7, "0") #Importe Actualizado
    cod += str(vencimiento2.strftime("%d%m%y")).rjust(6, "0") #Vencimiento2
    cod += str(liq.total).replace(".","").rjust(7, "0") #Importe2 Actualizado
    cod += str(liq.id_liquidacion).rjust(9, "0") #Id Liquidacion
    cod += str(liq.fecha.year).rjust(4, "0") #Anio
    cod += str(liq.fecha.month).rjust(2, "0") #Cuota    
    cod += str(digVerificador(cod))
    
    path = 'staticfiles/munis/'+settings.MUNI_DIR+'/'
    
    context['codbar'] = armarCodBar(cod)
    context['codigo'] = cod
    
    return render_to_pdf_response(request, template, context)