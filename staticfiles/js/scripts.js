$(document).ready(function(){           

function calcular(){        
        $('.form-actividades tr').each(function(j) {
            var $base = parseFloat($("input[name='actividades-"+j+"-base']").val());
            if ($base == '') $base=0;
    		var $alicuota = parseFloat($("input[name='actividades-"+j+"-alicuota']").val());
            if ($alicuota == '') $alicuota=0;
    		var $minimo = parseFloat($("input[name='actividades-"+j+"-minimo']").val());
            if ($minimo == '') $minimo=0; 

            var $aloc_coef = parseFloat($("input[name='alicuota_coeficiente']").val().replace(/,/, '.'));

            console.log($aloc_coef);
            var $total = parseFloat($base * $alicuota/$aloc_coef);
		    $total = $total.toFixed(2)

		    if($total <= $minimo){        
		        $(".totalActiv_"+j).text($minimo);
		        $("input[name='actividades-"+j+"-impuesto']").val($minimo);
                
		    }else{
		    	$(".totalActiv_"+j).text($total);
		    	$("input[name='actividades-"+j+"-impuesto']").val($total);	    	
		    }      
        });       
        return false;
    };

function CalcularTotal(){
      
    var $subTotal = 0.0;
    var $totalTotal = 0.0;

    calcular();
    var $minimoBol = parseFloat($("input[name='minimo_boleta']").val().replace(/,/, '.'));
        
    console.log($minimoBol);
   
    $('.form-actividades tr').each(function(j) {
        $subTotal += parseFloat($(".totalActiv_"+j).text()) ;
    });
    
    if ($subTotal <= $minimoBol){ $subTotal=$minimoBol;};
    
    var $totalTotal = 0.0;
    $('.Subtotal').text(parseFloat($subTotal).toFixed(2) );

    //Calculo el valor de los punitorios de la cuota
    var $cuota = $("input[name='id_cuota']").val();
    var $subtot = parseFloat($subTotal).toFixed(2);
    
    $.ajax({
        url: "/punitorios/"+$cuota+"/",
        type: "get", // or "get"       
        success: function(data) {
            var $porc = parseFloat(data).toFixed(3);
            var $rec = 0;
            $rec = $subtot * $porc;
            $rec = parseFloat($rec).toFixed(2);
            $("input[name='recargo']").val($rec);
             console.log($subtot);
             console.log($porc);
             console.log($rec);

            var $recargo = parseFloat($("input[name='recargo']").val());
            if ($recargo == '') $recargo=0;       
            var $derecho_neto = parseFloat($("input[name='derecho_neto']").val());
            if ($derecho_neto == '') $derecho_neto=0;                
            var $tasa_salud_publ = parseFloat($("input[name='tasa_salud_publ']").val());
            if ($tasa_salud_publ == '') $tasa_salud_publ=0;                 
            var $adic_monto = parseFloat($("input[name='adic_monto']").val());
            if ($adic_monto == '') $adic_monto=0;             
            var $retenciones = parseFloat($("input[name='retenciones']").val());
            if ($retenciones == '') $retenciones=0;                    
            
            $totalTotal = $subTotal+$recargo+$derecho_neto+$tasa_salud_publ+$adic_monto+$retenciones;

            $('.totalFinal').text(parseFloat($totalTotal).toFixed(2)  );
            $("input[id='id_total']").val(parseFloat($(".totalFinal").text()).toFixed(2));    
                }});

    // $('.form-adicionales tr').each(function(j) {
    //     $totalTotal += parseFloat($(".totalAdic_"+j).text()) ;
    // });
    //console.log($grandTotal);   
    
};

$("select[name='adic_select']").change(function(){    
    var $porc = parseFloat($("select[name='adic_select']").val())/100;
    var $subtot = parseFloat($('.Subtotal').text());
    var $tot = ($porc*$subtot).toFixed(2);
    $("input[name='adic_monto']").val($tot);
    $("input[name='adic_detalle']").val($(this).find("option:selected").text());
    CalcularTotal();  
    });

$('.form-actividades tr').each(function(j) {
    $("input").change(function(){
    CalcularTotal();      
     });
});


$("#recalcular").click(function(){
    CalcularTotal();    
     });

CalcularTotal();

function guardar(){
    CalcularTotal();
    return confirm('clicked');    
     };

    
});

