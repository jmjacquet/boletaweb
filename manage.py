#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "boletaWeb.settings")
    # os.environ['MUNI_ID'] = '865'
    # os.environ['MUNI_DB'] = 'gg_calchaqui'
    # os.environ['MUNI_DIR'] = 'calchaqui'
    os.environ['MUNI_ID'] = '000'
    os.environ['MUNI_DB'] = 'gg_prueba'
    os.environ['MUNI_DIR'] = 'prueba'
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
